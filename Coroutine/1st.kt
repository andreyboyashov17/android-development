import kotlinx.coroutines.*

suspend fun doWork()
{
    delay(999L)
    print("World")
}

suspend fun main()= coroutineScope{
    launch{ doWork() }
    delay(999L)
    print("Hello,")
}
